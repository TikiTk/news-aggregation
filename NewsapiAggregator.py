from newsapi import NewsApiClient
import json

newsapi = NewsApiClient(api_key='2ca66be17a834143bb31f7cef6c921a2')


def get_headlines(query_value, sources='google-news,news24,bbc-news,cnn,al-jazeera-english', language='en'):
    top_headlines = newsapi.get_top_headlines(q=query_value, sources=sources, language=language)
    return top_headlines


def get_all_articles(query_value, from_date, to_date, sources='google-news,news24,bbc-news,cnn,al-jazeera-english'):
    all_articles = newsapi.get_everything(q=query_value, sources=sources, from_param=from_date, to=to_date,
                                          language='en', sort_by='relevancy', page=2)
    return all_articles


def print_as_json(pull_data_from_api):
    articles = 'articles'
    for i in pull_data_from_api[('%s' % articles)]:
        print(json.dumps(i, indent=2, sort_keys=True))


def extract_content(pull_data_from_api):
    articles = 'articles'
    index = 'content'
    content_from_articles = []
    for i in pull_data_from_api[('%s' % articles)]:
        content_from_articles.append(i[index])

    return content_from_articles


# print_as_json(get_headlines('Kieswetter'))


# def extract content_from_news_feeds(news_feed_json):

# Date format yyyy-mm-dd
print_as_json(get_all_articles("Mosque",'2019-02-28','2019-03-29'))

# print(len(extract_content(get_all_articles("Discovery", '2019-02-28', '2019-03-29'))))
