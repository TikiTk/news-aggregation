import re

REPLACE_NO_SPACE = re.comple("[.;:!\'?,\"()\[\]]")
REPLACE_WITH_SPACE = re.compile("(<br\s*/><br\s*/>)|(\-)|(\/)")




def preprocess_text(reviews):
    reviews = [REPLACE_NO_SPACE.sub("",line.lower()) for line in reviews]
    reviews = [REPLACE_WITH_SPACE.sub(" ",line.lower()) for line in reviews]
    return reviews

review_train = []

for line in open("/home/tmphahlele/Downloads/aclImdb_v1/aclImdb/movie_data/full_train.txt","r"):
    review_train.append(line.strip())


reviews_test = []
for line in open("/home/tmphahlele/Downloads/aclImdb_v1/aclImdb/movie_data/full_test.txt","r"):
    reviews_test.append(line.strip())

processed_reviews_training = preprocess_text(review_train)
processed_reviews_testing = preprocess_text(reviews_test)
